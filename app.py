#!/usr/bin/env python
""" Flask API to determine whether the feature is a pothole

"""
from flask import Flask
from flask import render_template, abort, jsonify, make_response, request, url_for
from flask.ext.httpauth import HTTPBasicAuth
import MySQLdb as mdb

"""Create User Credentials"""
auth = HTTPBasicAuth()

pothole_data = [
    {
        'id': 1,
        'timestamp': '2012-04-23T18:25:43.511Z',
        'lat': 31.21,
        'lng': -100.1, 
        'speed': 100,
        'is_pothole': True
    },
    {
        'id': 1,
        'timestamp': '2012-04-23T18:25:43.511Z',
        'lat': 31.21,
        'lng': -100.1, 
        'speed': 100,
        'is_pothole': True
    }
]



@auth.get_password
def get_password(username):
    """This the the username and password for the app to compare
        curl -u pothole_app:passw0rd -i http://localhost:5000/pothole/api/v1.0/potholes/1
    """
    if username == 'pothole_app':
        return 'passw0rd'
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized'}), 403)


app = Flask(__name__)


@app.route('/pothole/api/v1.0/potholes', methods=['GET'])
@auth.login_required
def get_potholes():
    return jsonify({'potholes': [make_public_task(entry) for entry in pothole_data]})


@app.route('/pothole/api/v1.0/potholes/<int:entry_id>', methods=['GET'])
@auth.login_required
def get_pothole(entry_id):
    """ Get a single entry of a pothole based on its ID
    """
    entry = [entry for entry in pothole_data if entry['id'] == entry_id]
    if len(entry) == 0:
        abort(404)
    return jsonify({'pothole': make_public_task(entry[0])})


@app.route('/pothole/api/v1.0/potholes', methods=['POST'])
@auth.login_required
def create_pothole():
    """ Android app POSTs data and we verify whether it is a pothole here

    """
    #import pothole_checker as pc

    if not request.json or not 'lat' in request.json:
        abort(400)
    entry = {
        'id': pothole_data[-1]['id'] + 1,
        'lat': request.json['lat'],
        'lng': request.json['lng'],
        'speed': request.json['speed'],
        'data': request.json['data']
    }

    is_pothole = pc.isPothole(entry)
    
    if is_pothole:
        conn = lite.connect('pothole.db')
        c = conn.cursort("")

    pothole_data.append(entry)
    return jsonify({'pothole': entry}), 201


def make_public_task(entry):
    new_entry = {}
    for field in entry:
        if field == 'id':
            new_entry['uri'] = url_for('get_pothole', entry_id=entry['id'], _external=True)
        else:
            new_entry[field] = entry[field]
    return new_entry


@app.route('/map')
def plot_holes():
    """Map the potholes reported stored in the mysql db
    """

    coords = [{     "type": "Feature",
                    "properties": {
                        "timestamp": ["2012-04-23T18:25:43.511Z","2012-05-23T18:25:43.511Z","2012-06-23T18:25:43.511Z","2012-07-23T18:25:43.511Z"],
                        "accel": { "x":[1,2,3,4],
                                    "y":[1,2,3,4],
                                    "z":[1,2,3,4]
                        },
                        "speed": 50
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [-122.23,37.75]
                    }
                },
                {     "type": "Feature",
                    "properties": {
                        "timestamp": ["2012-04-23T18:25:43.511Z","2012-05-23T18:25:43.511Z","2012-06-23T18:25:43.511Z","2012-07-23T18:25:43.511Z"],
                        "accel": { "x":[1,2,3,4],
                                    "y":[1,2,3,4],
                                    "z":[1,2,3,4]
                        },
                        "speed": 100
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [-122.34,37.75]
                    }
                },
                {     "type": "Feature",
                    "properties": {
                        "timestamp": ["2012-04-23T18:25:43.511Z","2012-05-23T18:25:43.511Z","2012-06-23T18:25:43.511Z","2012-07-23T18:25:43.511Z"],
                        "accel": { "x":[1,2,3,4],
                                    "y":[1,2,3,4],
                                    "z":[1,2,3,4]
                        },
                        "speed": 150
                    },
                    "geometry": {
                        "type": "Point",
                        "coordinates": [-122.34,37.75]
                    }
                }
        ]

    return render_template('pothole_map.html',coords=coords)


@app.errorhandler(404)
def  not_found(error):
    return make_response(jsonify({'error':'Not found'}), 404)


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

if __name__ == '__main__':
    app.run(debug=True)



