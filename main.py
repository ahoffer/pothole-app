#!/usr/bin/env python
""" Flask API to determine whether the feature is a pothole

"""
from flask import Flask
from flask import render_template, abort, jsonify, make_response, request, url_for
from flask.ext.httpauth import HTTPBasicAuth
import MySQLdb
import json
import os


"""Create User Credentials"""
auth = HTTPBasicAuth()

@auth.get_password
def get_password(username):
    """This the the username and password for the app to compare
        curl -u pothole_app:passw0rd -i http://localhost:5000/pothole/api/v1.0/potholes/1
    """
    if username == 'pothole_app':
        return 'passw0rd'
    return None


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized'}), 403)


app = Flask(__name__)


# @app.route('/pothole/api/v1.0/potholes', methods=['GET'])
# #@auth.login_required
# def get_potholes():
#     return jsonify({'potholes': [make_public_task(entry) for entry in pothole_data]})


# @app.route('/pothole/api/v1.0/potholes/<int:entry_id>', methods=['GET'])
# #@auth.login_required
# def get_pothole(entry_id):
#     """ Get a single entry of a pothole based on its ID
#     """
#     entry = [entry for entry in pothole_data if entry['id'] == entry_id]
#     if len(entry) == 0:
#         abort(404)
#     return jsonify({'pothole': make_public_task(entry[0])})


@app.route('/pothole/api/v1.0/potholes', methods=['POST'])
#@auth.login_required
def create_pothole():
    """ Android app POSTs data and we verify whether it is a pothole here

    curl -H "Content-Type: application/json" -X POST -d '{"id":5,"latitude":35.7,"longitude":-122.11,"speed":33.1}' http://localhost:8080/pothole/api/v1.0/potholes

    """
    #import pothole_checker as pc
    if not request.json or not 'latitude' in request.json:
        abort(400)
    
    add_pothole = ("INSERT INTO potholeapp.mypotholes "
              "( timestamp, accel_x, accel_y, accel_z, latitude, longitude, speed, heading ) "
              "VALUES (%(timestamp)s, %(accel_x)s, %(accel_y)s, %(accel_z)s, %(latitude)s, %(longitude)s, %(speed)s, %(heading)s)")

    entry = {
        'timestamp': request.json['timestamp'],
        'accel_x': request.json['accel_x'],
        'accel_y': request.json['accel_y'],
        'accel_z': request.json['accel_z'],        
        'latitude': request.json['latitude'],
        'longitude': request.json['longitude'],
        'speed': request.json['speed'],
        'heading': request.json['heading']
    }

    # Define your production Cloud SQL instance information.
    _INSTANCE_NAME = 'potholeapp-1073:pothole'

    if (os.getenv('SERVER_SOFTWARE') and
        os.getenv('SERVER_SOFTWARE').startswith('Google App Engine/')):
        db = MySQLdb.connect(unix_socket='/cloudsql/' + _INSTANCE_NAME, db='potholeapp', user='root', charset='utf8')
    else:
        db = MySQLdb.connect(host='127.0.0.1', port=3306, db='potholeapp', user='root', charset='utf8')

    cursor = db.cursor()

    cursor.execute(add_pothole,entry)
    db.commit()
    # Check if it's a pothole and add to the database
    # is_pothole = pc.isPothole(entry)
    
    # if is_pothole:
    #     ##Add to database

    return jsonify({'pothole': entry}), 201


def make_public_task(entry):
    """ Return the full URI as part of the response to make the API more clear
    """
    new_entry = {}
    for field in entry:
        if field == 'id':
            new_entry['uri'] = url_for('get_pothole', entry_id=entry['id'], _external=True)
        else:
            new_entry[field] = entry[field]
    return new_entry


@app.route('/map')
def plot_holes():
    """Map the potholes reported stored in the mysql db
    """

    # Define your production Cloud SQL instance information.
    _INSTANCE_NAME = 'potholeapp-1073:pothole'

    if (os.getenv('SERVER_SOFTWARE') and
        os.getenv('SERVER_SOFTWARE').startswith('Google App Engine/')):
        db = MySQLdb.connect(unix_socket='/cloudsql/' + _INSTANCE_NAME, db='potholeapp', user='root', charset='utf8')
    else:
        db = MySQLdb.connect(host='127.0.0.1', port=3306, db='potholeapp', user='root', charset='utf8')

    cursor = db.cursor()

    lag_query = """select timestamp,accel_x,accel_y,accel_z,latitude,longitude,speed,heading,previous_accel_z from (
                    select
                    y.*
                    , @prev AS previous_accel_z
                    , @prev := accel_z
                    from
                    potholeapp.mypotholes y
                    , (select @prev:=NULL) vars
                    order by timestamp
                    ) subquery_alias
                    where speed > 10 and abs(accel_z - previous_accel_z) > 0.8
                    order by abs(accel_z) desc
                    LIMIT 1000
                """

    cursor.execute(lag_query)
    
    coords = []
    for row in cursor.fetchall():
        coords.append({"type": "Feature",\
                        "properties": {\
                        "timestamp":str(row[0]), "accel_x":float(row[1]), "accel_y":float(row[2]), "accel_z":float(row[3]), \
                        "speed":float(row[6]),"heading":float(row[7])\
                        },\
                        "geometry": {\
                            "type": "Point",\
                            "coordinates": [float(row[5]),float(row[4])]\
                            }\
                        })
        print(float(row[3])-float(row[8]))

    #internally test if it returns the results
    print(coords)

    return render_template('pothole_map.html',coords=coords)


@app.errorhandler(404)
def  not_found(error):
    return make_response(jsonify({'error':'Not found'}), 404)


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)

if __name__ == '__main__':
    app.run(debug=True)



